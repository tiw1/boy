package com.pakawat.boyapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.pakawat.boyapplication.databinding.FragmentOBinding
import com.pakawat.boyapplication.databinding.FragmentYBinding

class Y : Fragment() {
    private var _binding: FragmentYBinding? = null
    private val binding get() = _binding!!
    private lateinit var boy : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        boy = arguments?.getString(LETTER).toString() //ได้ bo

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentYBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonY.setOnClickListener {
            boy += "y" // +y = boy
            val action = YDirections.actionYToBOY(letter = "$boy")
            view.findNavController().navigate(action)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    companion object {
       const val LETTER = "letter"
    }
}