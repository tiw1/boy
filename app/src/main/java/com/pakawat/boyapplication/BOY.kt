package com.pakawat.boyapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.pakawat.boyapplication.databinding.FragmentBOYBinding
import com.pakawat.boyapplication.databinding.FragmentYBinding


class BOY : Fragment() {
    private var _binding: FragmentBOYBinding? = null
    private val binding get() = _binding!!
    private lateinit var boy : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        boy = arguments?.getString(Y.LETTER).toString() //ได้ boy
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBOYBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.result.text = boy

    }



}