package com.pakawat.boyapplication

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.pakawat.boyapplication.databinding.FragmentBBinding
import com.pakawat.boyapplication.databinding.FragmentOBinding


class O : Fragment() {
    private var _binding: FragmentOBinding? = null
    private val binding get() = _binding!!
    private lateinit var bo : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bo = arguments?.getString(LETTER).toString() //ได้ b


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonO.setOnClickListener {
            bo += "o" // +o = bo
            val action = ODirections.actionOToY(letter = "$bo")
            view.findNavController().navigate(action)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        const val LETTER = "letter"

    }
}